package com.gitee.sdk;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.lang.StringUtils;

import javax.sql.DataSource;
import java.io.*;
import java.util.Arrays;
import java.util.Properties;

/**
 * 文档生成放在 open-sdk-tool/数据库结构文档.doc
 *
 * @author: mengxin
 * @date: 2023/11/2 9:05
 */
public class ScrewMain {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            BufferedReader reader = ResourceUtil.getUtf8Reader(System.getProperty("user.dir") + "/jdbc.properties");
            properties.load(reader);
        }catch (IOException e){
            e.printStackTrace();
        }
        String title = properties.getProperty("title");
        if(StringUtils.isBlank(title)){
            title = "数据库结构文档";
        }
        String version = properties.getProperty("version");
        if(StringUtils.isBlank(version)){
            version = "1.0.0";
        }
        // 1.获取数据源
        DataSource dataSource = getDataSource(properties);
        // 2.获取数据库文档生成配置（文件路径、文件类型）
        EngineConfig engineConfig = getEngineConfig(title);
        // 3.获取数据库表的处理配置，可忽略
        ProcessConfig processConfig = getProcessConfig(properties);
        // 4.Screw 完整配置
        Configuration config = getScrewConfig(dataSource, engineConfig, processConfig, version);
        // 5.执行生成数据库文档
        new DocumentationExecute(config).execute();
    }

    /**
     * 获取数据库源
     */
    private static DataSource getDataSource(Properties properties) {
        //数据源
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(properties.getProperty("jdbc.driverClassName"));
        config.setJdbcUrl(properties.getProperty("jdbc.url"));
        config.setUsername(properties.getProperty("jdbc.username"));
        config.setPassword(properties.getProperty("jdbc.password"));
        //设置可以获取tables remarks信息
        config.addDataSourceProperty("useInformationSchema", "true");
        config.setMinimumIdle(2);
        config.setMaximumPoolSize(5);
        return new HikariDataSource(config);
    }

    /**
     * 获取文件生成配置
     */
    private static EngineConfig getEngineConfig(String title) {
        //生成配置
        return EngineConfig.builder()
                //生成文件路径：当前应用下 /doc/xx
                .fileOutputDir(System.getProperty("user.dir"))
                //打开目录
                .openOutputDir(true)
                //文件类型 HTML  WORD  MD
                .fileType(EngineFileType.WORD)
                //生成模板实现
                .produceType(EngineTemplateType.freemarker)
                //自定义文件名称
                .fileName(title).build();
    }

    /**
     * 获取数据库表的处理配置，可忽略
     */
    private static ProcessConfig getProcessConfig(Properties properties) {
        String tableName = properties.getProperty("ignoreTableName");
        String tablePrefix = properties.getProperty("ignoreTablePrefix");
        String tableSuffix = properties.getProperty("ignoreTableSuffix");
        return ProcessConfig.builder()
                //忽略表名
                .ignoreTableName(Arrays.asList(tableName.split(",")))
                //忽略表前缀
                .ignoreTablePrefix(Arrays.asList(tablePrefix.split(",")))
                //忽略表后缀
                .ignoreTableSuffix(Arrays.asList(tableSuffix.split(",")))
                .build();
    }

    private static Configuration getScrewConfig(DataSource dataSource, EngineConfig engineConfig, ProcessConfig processConfig, String version) {
        return Configuration.builder()
                //版本
                .version(version)
                //描述
                .description("数据库设计文档生成")
                //数据源
                .dataSource(dataSource)
                //生成配置
                .engineConfig(engineConfig)
                //生成配置
                .produceConfig(processConfig)
                .build();
    }
}
